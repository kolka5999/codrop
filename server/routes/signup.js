import { Router } from 'express'
import {createUser, findBeforeRegister} from '../models/users.js'
import {generateToken} from "../utils/generate.js";
const router = Router()

router.post('/signup', async (req, res)=> {
    try {
        const user = req.body
        const errorMessage = await findBeforeRegister(user)
        if (errorMessage) {
            return res.status(400).json({message: errorMessage})
        }
        const newUser = await createUser(req.body)
        const token = await generateToken({
            _key: newUser.new._key,
            userName: newUser.new.userName,
            password: newUser.new.password,
        })
        return res.status(201).json({
            id: newUser.new._key,
            token
        })
    } catch (e) {
      res.status(500).json(e)
    }
})

export default router
