import {Router} from 'express'
import passport from "passport";
import {generateToken} from "../utils/generate.js";
import {userAuth} from "../models/users.js";

const router = Router()

router.post('/login', async (req, res) => {
    try {
        const {username, password} = req.body
        const user = await userAuth({username, password})
        if (!user) {
            return res.status(401).json({
                message: 'this combination of login/password is not valid, please check it and try again'
            })
        }
        const token = await generateToken({
            _key: user._key,
            userName: user.userName,
            password: user.password
        })
        return res.status(200).json({
            id: user._key,
            token
        })
    } catch (e) {
        res.status(500).json(e)
    }
})

export default router
