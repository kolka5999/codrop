import { Router } from 'express'
import passport from "passport";
const router = Router()

router.get('/auth', passport.authenticate('jwt', {session: false}), async (req, res)=> {
    try {
        return res.status(200).json(true)
    } catch (e) {
        res.status(500).json(e)
    }
})

export default router
