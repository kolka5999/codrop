import {Router} from 'express'
import sgMail from '@sendgrid/mail'
import {findByEmail} from "../models/users.js";
import {generateToken} from "../utils/generate.js";

const router = Router()
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

router.post('/send-email', async (req, res) => {
    try {
        const email = req.body.email
        const user = await findByEmail(email)
        if (!user) {
            return res.status(400).json({
                message: 'User not found'
            })
        }
        const token = generateToken({
            _key: user._key,
            userName: user.userName,
            password: user.password
        })
        await sgMail.send({
            to: req.body.email,
            from: process.env.SENDGRID_FROM_EMAIL,
            subject: 'Sending with SendGrid',
            html: `<a href="${req.headers.referer}forgot-password?token=${token}">click here</a>`,
        });
        return res.status(200).json({
            message: 'message was sent'
        })
    } catch (e) {
        res.status(500).json(e)
    }
})

export default router
