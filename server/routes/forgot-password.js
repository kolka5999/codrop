import {Router} from 'express'
import passport from "passport";
import {updatePassword} from "../models/users.js";
import {generateToken} from "../utils/generate.js";

const router = Router()
router.post('/forgot-password',passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const newUser = await updatePassword(req.headers.authorization.replace('Bearer ', ''), req.body.password)
        const token = await generateToken({
            _key: newUser.new._key,
            userName: newUser.new.userName,
            password: newUser.new.password
        })
        return res.status(200).json({token})
    } catch (e) {
        res.status(500).json(e)
    }
})

export default router
