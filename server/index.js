import express from 'express'
import bodyParser from "body-parser";
import passport from 'passport'
import cors  from 'cors'
import signup from "./routes/signup.js";
import login from "./routes/login.js";
import auth from "./routes/auth.js";
import sendEmail from "./routes/send-email.js";
import forgotPassword from "./routes/forgot-password.js";
import {useJWTStrategy} from "./utils/pasport.js";

const app = express()
const PORT = process.env.PORT ?? 3000

useJWTStrategy(passport)

app.use(cors())
app.use(bodyParser.json())
app.use(signup)
app.use(login)
app.use(auth)
app.use(sendEmail)
app.use(forgotPassword)

app.listen(PORT, ()=> {})
