import {ExtractJwt, Strategy} from 'passport-jwt'
import {findByKey} from '../models/users.js'

export function useJWTStrategy(passport) {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.SECRET_KEY
    }, async (jwt_payload, done)=> {
        const user = await findByKey(jwt_payload._key)
        if (user) {
            return done(null, user);
        }
        return done(null, false);
    }));
}
