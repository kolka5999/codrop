import jwt from "jsonwebtoken";

export const generateToken = (newUser) => jwt.sign(newUser, process.env.SECRET_KEY, {
    expiresIn: process.env.EXPIRES_IN
})

export const decodeToken = (token) => jwt.verify(token, process.env.SECRET_KEY)
