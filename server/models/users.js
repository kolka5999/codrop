import {db} from "../database/connect.js";
import {aql} from "arangojs";
import {decodeToken} from "../utils/generate.js";

const Users = db.collection("users");

export async function findBeforeRegister ({userName, email}) {
    if (!userName && !email) return
    const byUserName = await db.query(aql`
      FOR user IN ${Users}
      FILTER user.userName == ${userName} 
      RETURN user
    `);
    if (await byUserName.next()) {
        return 'This user already exists!'
    }
    const byEmail = await db.query(aql`
      FOR user IN ${Users}
      FILTER user.email == ${email}
      RETURN user
    `);
    if (await byEmail.next()) {
        return 'This e-mail already exists!'
    }
    return false
}

export async function findByEmail (email) {
    if (!email) return
    const cursor = await db.query(aql`
      FOR user IN ${Users}
      FILTER user.email == ${email}
      LIMIT 1
      RETURN user
    `);
    return await cursor.next()
}

export async function updatePassword (token, password) {
    if (!password && !token) return
    const user = await decodeToken(token);
    user.password = password
    await Users.update(user._key, user);
    return await Users.update(user._key, user, { returnNew: true})
}

export async function createUser (user) {
    return await Users.save(user, { returnNew: true});
}

export async function findByKey(key) {
    if (!key) return;
    const cursor = await db.query(aql`
      FOR user IN ${Users}
      FILTER user._key == ${key}
      LIMIT 1
      RETURN user
    `);
    return await cursor.next()
}

export async function userAuth({username, password}) {
    if (!username && !password) return
    const cursor = await db.query(aql`
      FOR user IN ${Users}
      FILTER user.userName == ${username} && user.password == ${password}
      LIMIT 1
      RETURN user
    `);
    return await cursor.next()
}
