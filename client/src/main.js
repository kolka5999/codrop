import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import Vuikit from 'vuikit'
import VuikitIcons from '@vuikit/icons'
import '@vuikit/theme'
import './assets/style.css'
createApp(App)
    .use(Vuikit)
    .use(VuikitIcons)
    .use(router)
    .mount('#app')
