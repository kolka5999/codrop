import {createRouter, createWebHistory} from 'vue-router'
import Register from '../views/Register.vue'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Forgot from '../views/Forgot.vue'
import ForgotPassword from '../views/ForgotPassword.vue'
import Dashboard from '../views/Dashboard.vue'
import UserBilling from '../views/UserBilling.vue'
import UserAccount from '../views/UserAccount.vue'
import Error from '../views/Error.vue'
import Success from '../views/Success.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/dashboard',
        meta: {
            layout: 'dashboard',
            auth: true
        },
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path: '/user/account',
        meta: {
            layout: 'dashboard',
            auth: true
        },
        name: 'UserAccount',
        component: UserAccount
    },
    {
        path: '/user/billing',
        meta: {
            layout: 'dashboard',
            auth: true
        },
        name: 'UserBilling',
        component: UserBilling
    },
    {
        path: '/error',
        meta: {
            layout: 'dashboard',
            auth: true
        },
        name: 'Error',
        component: Error
    },
    {
        path: '/success',
        meta: {
            layout: 'dashboard',
            auth: true
        },
        name: 'Success',
        component: Success
    },
    {
        path: '/login',
        meta: {
            noAuth: true
        },
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        meta: {
            noAuth: true
        },
        name: 'Register',
        component: Register
    },
    {
        path: '/forgot',
        meta: {
            noAuth: true
        },
        name: 'Forgot',
        component: Forgot
    },
    {
        path: '/forgot-password',
        meta: {
            noAuth: true
        },
        name: 'ForgotPassword',
        component: ForgotPassword
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach(async (to, from, next) => {
    const apiUrl = process.env.VUE_APP_API_URL
    const response = await fetch(apiUrl + '/auth', {
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('jwt_token'),
        }
    })
    if (to.meta.noAuth) {
        if (response.status !== 401) {
            next('/dashboard')
            return
        }
    }
    if (to.meta.auth) {
        if (response.status === 401) {
            next('/login')
            return
        }
    }
    next()
})

export default router
